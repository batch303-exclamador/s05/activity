--SOLUTION 1
SELECT customerName 
FROM customers 
WHERE country = "Philippines";

--SOLUTION 2
SELECT contactLastName ,contactFirstName 
FROM customers 
WHERE customerName ="La Rochelle Gifts";

--SOLUTION 3
SELECT productName, MSRP 
FROM products 
WHERE productName  = "The Titanic";

--SOLUTION 4
SELECT firstName, lastName 
FROM employees 
WHERE email = "jfirrelli@classicmodelcars.com";

--SOLUTION 5
SELECT customerName 
FROM customers 
WHERE state IS NULL;

--SOLUTION 6
SELECT firstName, lastName, email 
FROM employees 
WHERE lastName = "Patterson" AND  firstName ="Steve";

--SOLUTION 7
SELECT customerName ,country, creditLimit 
FROM customers 
WHERE country != "USA" AND creditLimits > 3000;

--SOLUTION 8
SELECT customerNumber 
FROM orders 
WHERE comments LIKE "%DHL%";

--SOLUTION 9
SELECT * 
FROM productlines
WHERE textDescription LIKE "%state of the art%"; 

--SOLUTION 10
SELECT DISTINCT country
FROM customers;

--SOLUTION 11
SELECT DISTINCT status
FROM orders;

--SOLUTION 12
SELECT customerName ,country FROM customers WHERE country IN ("USA","France","Canada");

--SOLUTION 13
SELECT employees.firstName ,employees.lastName,offices.city
 FROM offices
JOIN employees
ON  offices.officeCode = employees.officecode 
WHERE offices.city = "Tokyo";

--SOLUTION 14
SELECT DISTINCT customers.customerName
FROM customers 
JOIN employees  ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Thompson';

--SOLUTION 15
SELECT products.productName, customers.customerName
FROM products 
JOIN orderdetails  ON products.productCode = orderdetails.productCode
JOIN orders  ON orderdetails.orderNumber = orders.orderNumber
JOIN customers  ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = 'Baane Mini Imports';

--SOLUTION 16
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country 
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE customers.country = offices.country;

--SOLUTION 17
SELECT products.productName, products.quantityInStock
FROM products
JOIN productlines ON products.productLine = productlines.productLine
WHERE productlines.productLine = 'Planes' AND products.quantityInStock < 1000;

--SOLUTION 18
SELECT customers.customerName
FROM customers
WHERE customers.phone LIKE '%+81%';


